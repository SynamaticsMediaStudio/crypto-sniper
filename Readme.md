# Crypto Snippets

## Description
This project aims to provide convenient PHP snippets for retrieving information about cryptocurrencies from the CryptoCompare API. 

## Installation
1. Clone this repository `git clone https://github.com/vishnurajiam/crypto-snippets.git`
2. Navigate to the project directory `cd crypto-snippets`

## Usage
1. Include the `call.php` file in your project
2. You can use the `GetCurrencyData` function to retrieve the current price and other information for a given cryptocurrency symbol.
   For example, to retrieve the current price of Bitcoin: `GetCurrencyData("BTC")`
3. You can use the `GetCurrencies` function to retrieve a list of all supported cryptocurrencies.
   For example: `GetCurrencies()`

## API Reference
The API used in this project is the CryptoCompare API, which provides a wide range of cryptocurrency data and services. 

To learn more about the API, please visit the official CryptoCompare API documentation at [https://min-api.cryptocompare.com](https://min-api.cryptocompare.com)

## Contributing
1. Fork the repository
2. Create a new branch `git checkout -b feature/my-new-feature`
3. Make your changes and commit them `git commit -m "Add my new feature"`
4. Push your changes to your forked repository `git push origin feature/my-new-feature`
5. Create a new pull request

## License
This project is licensed under the [MIT License](LICENSE).

