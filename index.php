<!DOCTYPE html>
<html>
<head>
	<title>Sniper</title>
	<link rel="stylesheet" href="//stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.18/css/dataTables.bootstrap4.css">

	<script type="text/javascript" src="//code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="//stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
	<script src="//cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
	<script src="//cdn.datatables.net/1.10.18/js/dataTables.bootstrap4.min.js"></script>
<style type="text/css">
@keyframes placeHolderShimmer{
    0%{
        background-position: -468px 0
    }
    100%{
        background-position: 468px 0
    }
}
.animated-background {
    animation-duration: 1s;
    animation-fill-mode: forwards;
    animation-iteration-count: infinite;
    animation-name: placeHolderShimmer;
    animation-timing-function: linear;
    background: #f6f7f8;
    background: linear-gradient(to right, #eeeeee 8%, #dddddd 18%, #eeeeee 33%);
    background-size: 800px 104px;
    min-height: 50px;
    float:left;
    width:100%;
}
</style>
</head>
<?php $u = "f6edad35c84baa8ced1995fb57cc501b";?>
<script type="text/javascript">
	$(document).ready(function() {
		// Send AJAX request to get cryptocurrencies list
		$.ajax({
			url: '/call.php?f=GetCurrencies',
			type: 'GET',
			beforeSend:function(){
				// Show loading animation
				$('tbody').append('<tr><th colspan="3" class="animated-background"></th></tr>');					
				$('tbody').append('<tr><th colspan="3" class="animated-background"></th></tr>');				
				$('tbody').append('<tr><th colspan="3" class="animated-background"></th></tr>');
				
			}
		})
		.done(function(res) {
			res = res.items;
			// Clear table body
			$('tbody').html('');

			// Create a container for each cryptocurrency row
			var container = $('<tbody></tbody>');

			// Iterate over each cryptocurrency item
			$.each(res, function(index, val) {
				// Check if cryptocurrency is trading
				if (val.trading == true) {
					// Create a row for the cryptocurrency
					var row = $('<tr id=\"coin-'+val.id+'\"><th><div class=\"animated-background\" style=\"background-image:url(https://www.cryptocompare.com/'+val.imageUrl+');animation:none;background-position:center;background-size:50%;background-repeat:no-repeat;\"></div> '+val.name+'</th><td>'+val.symbol+'</td></tr>');

					// Add the row to the container
					container.append(row);

					// Send AJAX request to get cryptocurrency data
					$.getJSON('/call.php?f=GetCurrencyData&par='+val.symbol+'', function(json, textStatus) {
						json = json.DISPLAY[val.symbol];
						var price = '<td>'+json.BTC.PRICE+'</td>';

						// Add the price to the row
						$('#coin-'+val.id).append(price);
					});
				}
			});

			// Add the container to the table
			$('#table').append(container);
		})
		.fail(function(e) {
			console.log("error, %s", e.responseText);
		})
		.always(function() {
			console.log("complete");
		});
		
	});

</script>
<body class="container">	
	<div class="row">
		<div class="col-sm-12">
			<div class="py-2"></div>
			<h1 class="display-4">Coins List</h1>
			<div class="py-2"></div>
			<table class="table" id="table">
				<thead>
					<tr>
						<th>Coins</th>
						<th>Coin symbol</th>
						<th>Market Price</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</body>
</html>

