<?php

declare(strict_types=1);

/**
 * @return array<string, mixed>
 */
function GetCurrencies() {
	$response = new stdClass();
    $data = file_get_contents("https://min-api.cryptocompare.com/data/all/coinlist");
    $data = json_decode($data, true);
	foreach ($data['Data'] as $value) {
		$item = new stdClass();
		$item->id = $value['Id'];
		$item->name = $value['Name'];
		$item->symbol = $value['Symbol'];
		$item->fullName = $value['FullName'];
		$item->url = $value['Url'];
		$item->imageUrl = "https://www.cryptocompare.com/".$value['ImageUrl'];
		$item->trading = $value['IsTrading'];
		$response->items[] = $item;
	}

	return $response;

}

/**
 * @param string $currID
 * @return string
 */
function GetCurrencyData(string $currID): array {
    $data = file_get_contents("https://min-api.cryptocompare.com/data/pricemultifull?fsyms=$currID&tsyms=USD,BTC");
    return json_decode($data, true);
}

header("Access-Control-Allow-Origin: *");
header("content-type: application/json");

$d = isset($_GET['f']) && function_exists($_GET['f']) ? (isset($_GET['par']) ? $_GET['f']($_GET['par']) : $_GET['f']()) : "";
echo json_encode($d, JSON_PRETTY_PRINT);

